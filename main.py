import re
import csv
import os
from datetime import datetime
from shutil import copyfile

last_dir_log = 'last_csv'
input_file = 'PROM.log'
out_file = 'post-processing.csv'

if __name__ == '__main__':

    if not os.path.exists(input_file):
        raise FileNotFoundError(input_file)

    if not os.path.exists(last_dir_log):
        os.mkdir(last_dir_log)

    if os.path.exists(out_file):
        last_name_file = out_file.split('.')
        new_name_file = last_name_file[0] + f"({datetime.today()}).csv"
        copyfile(src=out_file, dst=last_dir_log + '/' + new_name_file)

    print("Grace: %s file not found to work continue" % out_file)

    with open(input_file, encoding='windows-1251') as file:
        with open(out_file, 'w+', newline='') as output:
            writer = csv.writer(output)
            for line in file.readlines():
                line_list = ','.join(re.split(r'\t+', line.replace('\n', ''))).split(',')
                writer.writerow(line_list)
            output.close()
        file.close()

    print("Successful completion of the script")
